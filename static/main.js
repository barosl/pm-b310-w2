var volts_el = document.getElementById('volts');
var amperes_el = document.getElementById('amperes');
var watts_el = document.getElementById('watts');
var watt_hours_el = document.getElementById('watt_hours');
var charge_el = document.getElementById('charge');

var CHART_SIZE = 10;
var chart = new Highcharts.Chart({
	chart: {
		renderTo: 'chart',
		type: 'spline'
/*
		events: {
			load: function() {
				var series = this.series[0];
			}
		}
*/
	},

	title: {
		text: 'Power consumption'
	},

	xAxis: {
		type: 'datetime',
		tickPixelInterval: 150
	},

	yAxis: {
		title: {
			text: 'Watts'
		}
/*
		plotLines: [{
			value: 0,
			width: 1,
			color: '#ff0000'
		}]
*/
	},

	tooltip: {
		pointFormat: '<span style="color: {series.color};">\u25CF</span> {series.name}: <b>{point.y:.1f} watts</b>'
/*
		formatter: function() {
			return this.series.name + ': <b>' + this.y.toFixed(1) + ' watts</b><br>Time: ' + this.x;
		}
*/
	},

	series: [{
		name: 'Power',
		data: POWER_SERIES
/*
		data: function() {
			var res = [];
			var cur_time = new Date().getTime();
			for (var i=0;i<CHART_SIZE;i++) {
				res.push([cur_time - (CHART_SIZE - i - 1)*1000, 0]);
			}
			return res;
		}()
*/
	}]
});

function apply(data) {
	volts_el.innerHTML = data.volts.toFixed(1) + ' volts';
	amperes_el.innerHTML = data.amperes.toFixed(1) + ' amperes';
	watts_el.innerHTML = data.watts.toFixed(1) + ' watts';
	watt_hours_el.innerHTML = data.watt_hours.toFixed(1) + ' watt-hours';
	charge_el.innerHTML = data.charge.toFixed(1) + ' won';

//	chart.series[0].addPoint([new Date().getTime(), data.watts], true, true);
	chart.series[0].addPoint([data.ts * 1000, data.watts], true, true);
}

var prev_data_id = 0;
function update() {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var data = JSON.parse(xhr.responseText);

		if (prev_data_id == data.id) return;
		prev_data_id = data.id;

		apply(data);
	};
	xhr.open('GET', API_URL);
	xhr.send();
}

setInterval(function() { update(); }, 500);
