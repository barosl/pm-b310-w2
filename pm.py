#!/usr/bin/env python3

import socket
import struct

from cfg import cfg

pkts = {
	'handshake':
		b'\xfa\x00\x00\x00\x00\x00' +
		b'\x00\x51' +
		b'\x13\x19\x10\xfb\x4a' +
		b'\r',

	'req': b'\xfa\x31\x41\x43' +
		b'\x31\x31\x45\x74' +
		b'\x50\x75\x73\x68\xfb\x3d' +
		b'\r',
}

PKT_HEADER = b'\xfa\x31\x41\x43'
PKT_HEADER_LEN = len(PKT_HEADER)
PKT_FOOTER = b'\r'
PKT_FOOTER_LEN = len(PKT_FOOTER)

PKT_RESPONSE = b'\x31\x31\x45\x40'
PKT_RESPONSE_LEN = len(PKT_RESPONSE)

def pkt_recv(sock):
	res = b''
	while True:
		buf = sock.recv(1)
		if not buf:
			raise Exception('Socket closed')
		res += buf

		if res[-PKT_HEADER_LEN:] == PKT_HEADER:
			res = res[:-PKT_HEADER_LEN]
			if not res: continue
			if res[-PKT_FOOTER_LEN:] != PKT_FOOTER:
				raise Exception('Invalid packet')
			return res[:-PKT_FOOTER_LEN]

def loop(handler):
	sock = socket.socket()
	sock.connect((cfg['host'], cfg['port']))

	sock.send(pkts['handshake'])

	while True:
		buf = pkt_recv(sock)

		if buf[:PKT_RESPONSE_LEN] == PKT_RESPONSE:
			sock.send(pkts['req'])
			sock.send(pkts['req']) # FIXME: If the request packet isn't sent twice, the server stops responding after a while. I don't know why!

			data = struct.unpack('<fffffffflllH', buf)

			volts = data[1]
			amperes = data[2]
			watts = data[5]
			watt_hours = data[6]
			charge = data[7]

			handler(
				volts=volts,
				amperes=amperes,
				watts=watts,
				watt_hours=watt_hours,
				charge=charge,
			)

def main():
	def display(volts, amperes, watts, watt_hours, charge):
		print('Volts: {}'.format(volts))
		print('Amperes: {}'.format(amperes))
		print('Watts: {}'.format(watts))
		print('Watt-hours: {}'.format(watt_hours))
		print('Charge: {}'.format(charge))

	loop(display)

if __name__ == '__main__':
	main()
