from flask import Flask, render_template
import pm
import threading
import json
import time

CHART_SIZE = 20

app = Flask(__name__)

pm_data = {}
pm_data_id = 0

pm_data_list = []

@app.route('/api/')
def api():
	return json.dumps(pm_data)

@app.route('/')
def index():
	power_series = [[x['ts'] * 1000, x['watts']] for x in pm_data_list]
	if not power_series: power_series = [[0, 0]]
	power_series = power_series[:1] * (CHART_SIZE - len(power_series)) + power_series

	return render_template('index.html',
		power_series=power_series,
	)

if __name__ == '__main__':
	def save(volts, amperes, watts, watt_hours, charge):
		global pm_data, pm_data_id

		pm_data_id += 1

		pm_data = {
			'id': pm_data_id,
			'ts': time.time(),

			'volts': volts,
			'amperes': amperes,
			'watts': watts,
			'watt_hours': watt_hours,
			'charge': charge,
		}

		pm_data_list.append(pm_data)
		pm_data_list[:-CHART_SIZE] = []

	threading.Thread(target=pm.loop, args=[save]).start()

	app.run(host='0.0.0.0', port=80, threaded=True)
